package com.example.levan_bujiashvili

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        loginButton.setOnClickListener {
            if (emailEditText.text.toString().isNotEmpty() && passwordEditText.text.toString().isNotEmpty()){
                Toast.makeText(this,"You're in", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this,"Fill all fields", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
